# FastBlog


### 介绍

个人入门练手项目

### 基本要求

* Python: 3.8
* FastAPI: 0.67.0
* Mysql: 8
* ......

## ⬇ 源码下载
```
wget https://gitee.com/wu_cl/FastBlog/repository/archive/master.zip
or
git clone https://gitee.com/wu_cl/FastBlog.git
```
敏感词文件内容 static/sensitive_words/sensitive_words_lines.txt, 
请前往 https://github.com/wjhgg/sensitive_words 进行替换

### 功能点

* 用户注册，登录，登出，注销，密码邮箱验证重置
* 文章分类，归档，快捷搜索，标签集
* 文章排行榜 / 最新评论 / 阅读量排行榜
* 博文评论系统
* 支持图床，后台上传图片可直链访问及调用
* .....

### 基本使用
```
mysql8 创建本地数据库，库名: fast, 编码选择: utf8mb4
```

#### backend:
```
first > pip install -r requirements.txt

1 > 修改 conf.py 文件中数据库配置

2 > 命令行进入 backend/app/ 目录下执行 > alembic revision --autogenerate

3 > 继续执行 > alembic upgrade head

4 > 运行 init_test_data.py 初始化用户数据

end > 运行 main.py 文件
```