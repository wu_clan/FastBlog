#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from typing import Any, Optional

from pydantic import BaseModel

"""
说明：统一响应状态码
"""


class ResponseBase(BaseModel):
    data: Any = None


class Response200(ResponseBase):
    code: int = 200
    msg: Optional[str] = None


class Response301(ResponseBase):
    code: int = 301
    msg: Optional[str] = None


class Response401(ResponseBase):
    code: int = 401
    msg: Optional[str] = None


class Response403(ResponseBase):
    code: int = 403
    msg: Optional[str] = None


class Response404(ResponseBase):
    code: int = 404
    msg: Optional[str] = None


class Response500(ResponseBase):
    code: int = 500
    msg: Optional[str] = None


class Response502(ResponseBase):
    code: int = 502
    msg: Optional[str] = None
