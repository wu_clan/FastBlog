#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from typing import Any, Optional

from pydantic import BaseModel


class Token(BaseModel):
    """返回的token类型"""
    code: Optional[int] = None
    msg: Optional[str] = None
    access_token: str
    token_type: str
    is_superuser: Optional[bool] = None
