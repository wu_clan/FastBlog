#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pydantic import BaseModel, Field


class CreateComment(BaseModel):
    user_name: str
    email: str = Field(..., example='user@example.com')
    url: str
    comment: str
