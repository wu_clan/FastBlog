#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pydantic import BaseModel


class TagBase(BaseModel):
    tag_name: str
