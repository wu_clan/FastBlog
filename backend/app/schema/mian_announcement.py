#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pydantic import BaseModel


class MainAnnounceBase(BaseModel):
    main_announcement: str
