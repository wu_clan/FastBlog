#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pydantic import BaseModel


class CreateSubscription(BaseModel):
    email: str


class DeleteSub(CreateSubscription):
    pass
