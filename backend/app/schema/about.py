#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pydantic import BaseModel


class CreatAbout(BaseModel):
    about_contents: str = None


class UpdateAbout(CreatAbout):
    pass
