#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from typing import Optional

from pydantic import BaseModel


class HeadAnnounceBase(BaseModel):
    head_announcement: str

