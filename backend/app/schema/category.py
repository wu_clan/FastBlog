#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from datetime import datetime

from pydantic import BaseModel


class CategoryBase(BaseModel):
    name: str


class CreateCategory(CategoryBase):
    pass
