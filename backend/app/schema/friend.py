#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pydantic import BaseModel


class CreateFriend(BaseModel):
    url: str
    title: str = None
    name: str


class UpdateFriend(CreateFriend):
    pass

