#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from typing import Optional

from pydantic import BaseModel, Field


class ArticleBase(BaseModel):
    title: str
    content: str = Field(..., example='文章正文')
    digest: Optional[str] = Field(..., example='文章摘要')
    view_num: Optional[int] = 0
    comment_num: Optional[int] = 0
    picture: Optional[str] = Field(..., example='图片链接')


class CreateArticle(ArticleBase):
    pass


class UpdateArticle(ArticleBase):
    pass
