#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from backend.app.core.conf import settings


def send_email_subscription(send_to, article_title, link):
    """
    邮箱文章订阅
    :param send_to: 收件人列表
    :param article_title: 文章title
    :param link: 文章链接
    :return:
    """
    message = MIMEMultipart()
    subject = '订阅推送'
    content = MIMEText(f'你订阅的 "{settings.DEFAULT_FROM_EMAIL}" 发布新文章了，快点击链接查阅吧\n文章：{article_title}\n链接：{link}',
                       _charset="utf-8")
    message['from'] = settings.EMAIL_USER
    message['subject'] = subject
    message.attach(content)

    # 登录smtp服务器并发送邮件
    smtp = smtplib.SMTP()
    smtp.connect(settings.EMAIL_SERVER)
    smtp.login(settings.EMAIL_USER, settings.EMAIL_PASSWORD)
    smtp.sendmail(message['from'], send_to, message.as_string())
    smtp.quit()
