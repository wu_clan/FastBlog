#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from fastapi import Form
from sqlalchemy.orm import Session

from backend.app.model import PictureBed


def get_img(db: Session) -> bool:
    return db.query(PictureBed)


def get_img_by_id(db: Session, pic_id: int) -> bool:
    return db.query(PictureBed).filter(PictureBed.id == pic_id).first()


def get_img_by_title(db: Session, title: str) -> bool:
    return db.query(PictureBed).filter(PictureBed.img_title == title).first()


def get_img_name(db: Session, picture_id: int) -> str:
    return db.query(PictureBed).filter(PictureBed.id == picture_id).first().images


def create_img(db: Session, post: str, images: str) -> PictureBed:
    pic_obj = PictureBed(img_title=post, images=images)
    db.add(pic_obj)
    db.commit()
    db.refresh(pic_obj)
    return pic_obj


def update_img(db: Session, pic_id: int, images: str, put: str = Form(...)) -> bool:
    pic_obj = db.query(PictureBed).filter(PictureBed.id == pic_id)
    pic_obj.update({
        'img_title': put,
        'images': images
    })
    db.commit()
    return pic_obj.first()


def delete_img(db: Session, pic_id: int) -> bool:
    pic_obj = db.query(PictureBed).filter(PictureBed.id == pic_id)
    pic_obj.delete()
    db.commit()
    return pic_obj.first()
