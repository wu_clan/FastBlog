#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy.orm import Session

from backend.app.model import Subscription
from backend.app.schema.subscription import CreateSubscription, DeleteSub


def check_sub_email(db: Session, sub: CreateSubscription) -> bool:
    return db.query(Subscription).filter(Subscription.email == sub.email).first()


def create_sub(db: Session, sub: CreateSubscription) -> Subscription:
    sub_obj = Subscription(**sub.dict())
    db.add(sub_obj)
    db.commit()
    db.refresh(sub_obj)
    return sub_obj


def delete_sub(db: Session, sub: DeleteSub) -> bool:
    data = db.query(Subscription).filter(Subscription.email == sub.email)
    data.delete()
    db.commit()
    return data.first()
