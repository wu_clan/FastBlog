#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy.orm import Session

from backend.app.model import Friend
from backend.app.schema.friend import CreateFriend


def get_friend(db: Session) -> bool:
    return db.query(Friend)


def get_friend_by_id(db: Session, friend_id: int) -> bool:
    return db.query(Friend).filter(Friend.id == friend_id).first()


def get_all(db: Session) -> list:
    return db.query(Friend).all()


def create_friend(db: Session, post: CreateFriend) -> Friend:
    friend = Friend(**post.dict())
    db.add(friend)
    db.commit()
    db.refresh(friend)
    return friend


def update_friend(db: Session, friend_id: int, update: CreateFriend) -> bool:
    friend = db.query(Friend).filter(Friend.id == friend_id)
    friend.update({'url': update.url, 'title': update.title, 'name': update.name})
    db.commit()
    return friend.first()


def delete_friend(db: Session, friend_id: int) -> bool:
    friend = db.query(Friend).filter(Friend.id == friend_id)
    friend.delete()
    db.commit()
    return friend.first()
