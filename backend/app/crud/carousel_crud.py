#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from typing import Optional

from sqlalchemy.orm import Session

from backend.app.model import Carousel


def get_carousel(db: Session) -> bool:
    return db.query(Carousel)


def get_carousel_by_id(db: Session, ca_id: int) -> bool:
    return db.query(Carousel).filter(Carousel.id == ca_id).first()


def get_carousel_text(db: Session, ca_id: int) -> str:
    return db.query(Carousel).filter(Carousel.id == ca_id).first().carousel


def create_carousel(db: Session,
                    carousel: str,
                    carousel_explanation: Optional[str],
                    img_title: Optional[str],
                    img_alt: Optional[str], ) -> Carousel:
    ca_obj = Carousel(carousel=carousel, carousel_explanation=carousel_explanation, img_title=img_title,
                      img_alt=img_alt)
    db.add(ca_obj)
    db.commit()
    db.refresh(ca_obj)
    return ca_obj


def update_carousel(db: Session,
                    carousel_id: int,
                    carousel: str,
                    carousel_explanation: Optional[str],
                    img_title: Optional[str],
                    img_alt: Optional[str], ) -> bool:
    ca = db.query(Carousel).filter(Carousel.id == carousel_id)
    ca.update({
        'carousel': carousel,
        'carousel_explanation': carousel_explanation,
        'img_title': img_title,
        'img_alt': img_alt
    })
    db.commit()
    return ca.first()


def delete_carousel(db: Session, carousel_id: int) -> bool:
    ca = db.query(Carousel).filter(Carousel.id == carousel_id)
    ca.delete()
    db.commit()
    return ca.first()
