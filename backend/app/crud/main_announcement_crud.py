#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy.orm import Session

from backend.app.model import MainAnnouncement
from backend.app.schema.mian_announcement import MainAnnounceBase


def get_am(db: Session) -> bool:
    return db.query(MainAnnouncement)


def get_am_by_id(db: Session, am_id: int) -> bool:
    return db.query(MainAnnouncement).filter(MainAnnouncement.id == am_id).first()


def create_am(db: Session, post: MainAnnounceBase) -> MainAnnouncement:
    text_obj = MainAnnouncement(**post.dict())
    db.add(text_obj)
    db.commit()
    db.refresh(text_obj)
    return text_obj


def update_am(db: Session, am_id: int, put: MainAnnounceBase) -> bool:
    am = db.query(MainAnnouncement).filter(MainAnnouncement.id == am_id)
    am.update({'head_announcement': put.main_announcement})
    db.commit()
    return am.first()


def delete_am(db: Session, am_id: int) -> bool:
    am = db.query(MainAnnouncement).filter(MainAnnouncement.id == am_id)
    am.delete()
    db.commit()
    return am.first()
