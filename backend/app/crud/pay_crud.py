#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy.orm import Session

from backend.app.model import Pay


def get_pay(db: Session) -> bool:
    return db.query(Pay)


def get_pay_by_id(db: Session, pay_id: int) -> bool:
    return db.query(Pay).filter(Pay.id == pay_id).first()


def get_pay_img_by_id(db: Session, pay_id: int) -> str:
    return db.query(Pay).filter(Pay.id == pay_id).first().pay_img


def create_pay(db: Session, post: str) -> Pay:
    pay_obj = Pay(pay_img=post)
    db.add(pay_obj)
    db.commit()
    db.refresh(pay_obj)
    return pay_obj


def update_pay(db: Session, pay_id: int, put: str) -> bool:
    pay = db.query(Pay).filter(Pay.id == pay_id)
    pay.update({'pay_img': put})
    db.commit()
    return pay.first()


def delete_pay(db: Session, pay_id: int) -> bool:
    pay = db.query(Pay).filter(Pay.id == pay_id)
    pay.delete()
    db.commit()
    return pay.first()
