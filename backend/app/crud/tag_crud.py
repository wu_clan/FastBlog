#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy.orm import Session

from backend.app.model import Tag
from backend.app.schema.tag import TagBase


def get_tag(db: Session) -> bool:
    return db.query(Tag)


def get_tags(db: Session) -> list:
    return db.query(Tag).all()


def get_tags_limit(db: Session, tag_id: int, page: int, limit: int):
    return db.query(Tag).filter(Tag.id == tag_id).limit(limit).offset((page - 1) * limit).all()


def get_tag_for_id(db: Session, tag_id: int) -> Tag:
    return db.query(Tag).get(tag_id)


def get_tag_by_id(db: Session, tag_id: int) -> bool:
    return db.query(Tag).get(tag_id)


def get_tag_by_name(db: Session, tag_name: str) -> bool:
    return db.query(Tag).filter(Tag.tag_name == tag_name).first()


def create_tag(db: Session, tag: TagBase) -> Tag:
    tag_obj = Tag(**tag.dict())
    db.add(tag_obj)
    db.commit()
    db.refresh(tag_obj)
    return tag_obj


def update_tag(db: Session, tag_id: int, update: TagBase) -> bool:
    tag = db.query(Tag).filter(Tag.id == tag_id)
    tag.update({'tag_name': update.tag_name})
    db.commit()
    return tag.first()


def delete_tag(db: Session, tag_id: int) -> bool:
    tag = db.query(Tag).filter(Tag.id == tag_id)
    tag.delete()
    db.commit()
    return tag.first()
