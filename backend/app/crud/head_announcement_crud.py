#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy.orm import Session

from backend.app.model import HeadAnnouncement
from backend.app.schema.head_announcement import HeadAnnounceBase


def get_am(db: Session) -> bool:
    return db.query(HeadAnnouncement)


def get_am_by_id(db: Session, am_id: int) -> bool:
    return db.query(HeadAnnouncement).filter(HeadAnnouncement.id == am_id).first()


def create_am(db: Session, post: HeadAnnounceBase) -> HeadAnnouncement:
    text_obj = HeadAnnouncement(**post.dict())
    db.add(text_obj)
    db.commit()
    db.refresh(text_obj)
    return text_obj


def update_am(db: Session, am_id: int, put: HeadAnnounceBase) -> bool:
    am = db.query(HeadAnnouncement).filter(HeadAnnouncement.id == am_id)
    am.update({'head_announcement': put.head_announcement})
    db.commit()
    return am.first()


def delete_am(db: Session, am_id: int) -> bool:
    am = db.query(HeadAnnouncement).filter(HeadAnnouncement.id == am_id)
    am.delete()
    db.commit()
    return am.first()
