#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import secrets
from functools import lru_cache
from typing import List

from pydantic import BaseSettings


class Settings(BaseSettings):
    """ 配置类 """
    # FastAPI
    TITLE: str = '博客系统API'
    VERSION: str = 'v0.0.1'
    DESCRIPTION: str = """
    使用 FastAPI 重写了Django-blog项目:DBlog
    > 地址：
    DBlog git: https://gitee.com/wu_cl/DBlog
    FastAPI git: https://gitee.com/wu_cl/FastBlog
    """
    DOCS_URL: str = '/v1/docs'
    OPENAPI_URL: str = '/v1/openapi'
    REDOCS_URL: bool = False

    # Uvicorn
    HOST: str = '127.0.0.1'
    PORT: int = 8000
    RELOAD: bool = True

    # DB
    DB_ECHO: bool = False
    DB_HOST: str = '127.0.0.1'
    DB_PORT: int = 3306
    DB_USER: str = 'root'
    DB_PASSWORD: str = '123456'
    DB_DATABASE: str = 'fast'
    DB_ENCODING: str = 'utf8mb4'

    # Token
    ALGORITHM: str = 'HS256'  # 算法
    SECRET_KEY: str = secrets.token_urlsafe(32)  # 随机密钥
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 3  # token 时效 60 * 24 * 3 = 3 天

    # 密码重置 cookies 过期时间
    MAX_AGE: int = 60 * 5  # cookies 时效 60 * 5 = 5 分钟

    # Email
    DEFAULT_FROM_EMAIL: str = 'xiaowu的个人博客'  # 默认发件说明
    EMAIL_SERVER: str = 'smtp.qq.com'
    EMAIL_USER: str = 'xiaowu-nav@qq.com'
    EMAIL_PASSWORD: str = 'cvszjyenrlvfkeaef'  # 授权密码，非邮箱密码


@lru_cache
def get_settings():
    """ 读取配置优化写法 """
    return Settings()


settings = get_settings()
