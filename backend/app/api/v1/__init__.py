#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from fastapi import APIRouter

from .user import user
from .blog import blog
from .admin import su

v1 = APIRouter(prefix='/v1')

v1.include_router(user, prefix='/user', tags=['用户'])
v1.include_router(blog, prefix='/blog', tags=['游客'])
v1.include_router(su, prefix='/admin', tags=['管理员/后台'])

