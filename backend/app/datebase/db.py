#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from backend.app.datebase.session import db_session


def get_db():
    """
    每一个请求处理完毕后会关闭当前连接，不同的请求使用不同的连接
    :return:
    """
    conn = db_session()
    try:
        yield conn
    finally:
        conn.close()
