#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# 导入所有模型，并将 Base 放在最前面， 以便 Base 拥有它们
# imported by Alembic
"""
from backend.app.datebase.base_class import Base
from backend.app.model.about import About
from backend.app.model.haed_announcement import HeadAnnouncement
from backend.app.model.main_announcement import MainAnnouncement
from backend.app.model.article import Article
from backend.app.model.article import ArticleTag
from backend.app.model.carousel import Carousel
from backend.app.model.category import Category
from backend.app.model.comment import Comment
from backend.app.model.friend import Friend
from backend.app.model.pay import Pay
from backend.app.model.picturebed import PictureBed
from backend.app.model.subscription import Subscription
from backend.app.model.tag import Tag
from backend.app.model.user import User
from backend.app.model.webconf import WebConf
