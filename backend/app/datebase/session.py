#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from backend.app.core.conf import settings
from backend.app.common.log import log

""" 
说明：SqlAlchemy
"""

SQLALCHEMY_DATABASE_URL = f'mysql+pymysql://{settings.DB_USER}:{settings.DB_PASSWORD}@{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_DATABASE}?charset={settings.DB_ENCODING}'

try:
    # 数据库引擎
    engine = create_engine(SQLALCHEMY_DATABASE_URL, echo=settings.DB_ECHO)
    # log.success('数据库连接成功')
except Exception as e:
    log.error('数据库链接失败')
    log.error(e)
else:
    # 创建会话（增删改查）
    db_session = sessionmaker(bind=engine, autocommit=False, autoflush=False)


if __name__ == '__main__':
    print(dir(engine))
    print(f'---session---', dir(db_session))
