#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, String

from backend.app.datebase.base_class import Base


class Tag(Base):
    """ 标签 """
    __tablename__ = 'tag'
    id = Column(Integer, primary_key=True, autoincrement=True)
    tag_name = Column(String(128), nullable=False, unique=True, comment='标签名称')
