#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, LargeBinary, String

from backend.app.datebase.base_class import Base


class PictureBed(Base):
    """ 网站图床 """
    __tablename__ = 'picturebed'
    id = Column(Integer, primary_key=True, autoincrement=True)
    img_title = Column(String(128), unique=True, nullable=False, comment='图片标题')
    images = Column(String(256), comment='图床图片')
