#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String

from backend.app.datebase.base_class import Base


class Friend(Base):
    """ 友链 """
    __tablename__ = 'friend'
    id = Column(Integer, primary_key=True, autoincrement=True)
    url = Column(String(256), nullable=False, comment='友链接')
    title = Column(String(128), comment='友链title')
    name = Column(String(128), nullable=False, comment='友联名称')
