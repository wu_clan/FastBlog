#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer
from sqlalchemy.dialects.mysql import LONGTEXT

from backend.app.datebase.base_class import Base


class About(Base):
    """ 关于 """
    __tablename__ = 'about'
    id = Column(Integer, primary_key=True, autoincrement=True)
    about_contents = Column(LONGTEXT, default=None, comment='关于text')
