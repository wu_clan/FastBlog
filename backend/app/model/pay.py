#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, LargeBinary, String

from backend.app.datebase.base_class import Base


class Pay(Base):
    """ 打赏图 """
    __tablename__ = 'pay'
    id = Column(Integer, primary_key=True, autoincrement=True)
    pay_img = Column(String(256), nullable=False, comment='打赏图')
