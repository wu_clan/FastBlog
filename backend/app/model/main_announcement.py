#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String

from backend.app.datebase.base_class import Base


class MainAnnouncement(Base):
    """ 公告 """
    __tablename__ = 'main_announcement'
    id = Column(Integer, primary_key=True, autoincrement=True)
    main_announcement = Column(String(300), nullable=False, comment='主公告')
