#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, LargeBinary, String, Text

from backend.app.datebase.base_class import Base


class WebConf(Base):
    """ 网站配置信息 """
    __tablename__ = 'webconf'
    id = Column(Integer, primary_key=True, autoincrement=True)
    website_address = Column(String(128), comment='网站地址')
    name = Column(String(128), comment='网站内用名')
    chinese_description = Column(String(128), comment='中文说明')
    english_description = Column(Text(100), comment='英文说明')
    avatar_url = Column(String(256), comment='头像链接')
    website_author = Column(String(128), comment='网站作者')
    website_author_url = Column(String(256), comment='网站作者链接')
    inbox = Column(String(128), comment='收件箱')
    website_police = Column(String(128), comment='网站备案')
    git = Column(String(128), comment='git地址')
    website_logo = Column(String(256), comment='网站logo')
