#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from typing import TYPE_CHECKING

from sqlalchemy import Column, DateTime, ForeignKey, func, Integer, String
from sqlalchemy.dialects.mysql import LONGTEXT
from sqlalchemy.orm import backref, relationship

from backend.app.datebase.base_class import Base

if TYPE_CHECKING:
    from .article import Article


class Comment(Base):
    """ 评论 """
    __tablename__ = 'comment'
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(128), nullable=False, comment='标题')
    create_time = Column(DateTime, server_default=func.now(), comment='发表时间')
    user_name = Column(String(128), nullable=False, comment='用户')
    email = Column(String(128), nullable=False, comment='邮箱')
    url = Column(String(1000), nullable=False, comment='链接')
    comment = Column(LONGTEXT, nullable=False, comment='评论内容')
    article_id = Column(Integer, ForeignKey('article.id', ondelete='CASCADE'))  # 评论，文章多对一
    article = relationship('Article', backref=backref('comments', cascade="all, delete"))
