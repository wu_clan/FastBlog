#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String

from backend.app.datebase.base_class import Base


class HeadAnnouncement(Base):
    """ 公告 """
    __tablename__ = 'head_announcement'
    id = Column(Integer, primary_key=True, autoincrement=True)
    head_announcement = Column(String(128), nullable=False, comment='轮播公告')
