#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, LargeBinary, String

from backend.app.datebase.base_class import Base


class Carousel(Base):
    """ 轮播图 """
    __tablename__ = 'carousel'
    id = Column(Integer, primary_key=True, autoincrement=True)
    carousel = Column(String(256), nullable=False, comment='轮播图')
    carousel_explanation = Column(String(128), comment='轮播图说明')
    img_title = Column(String(128), comment='图片标题')
    img_alt = Column(String(128), comment='图片alt')
