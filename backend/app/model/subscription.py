#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy import Column, DateTime, func, Integer, String

from backend.app.datebase.base_class import Base


class Subscription(Base):
    """ 邮箱订阅 """
    __tablename__ = 'subscription'
    id = Column(Integer, primary_key=True, autoincrement=True)
    email = Column(String(128), unique=True, nullable=False,  comment='邮箱')
    sub_time = Column(DateTime, server_default=func.now(), comment='订阅时间')
