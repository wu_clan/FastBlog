#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import datetime
from typing import TYPE_CHECKING

from sqlalchemy import Column, DateTime, ForeignKey, func, Integer, String
from sqlalchemy.orm import relationship

from backend.app.datebase.base_class import Base


class Category(Base):
    """ 文章类型 """
    __tablename__ = 'category'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(128), nullable=False, comment='文章类型')
    create_time = Column(DateTime, default=datetime.datetime.now(), comment='创建时间')
    last_mod_time = Column(DateTime, server_default=func.now(), onupdate=func.now(), comment='修改时间')
